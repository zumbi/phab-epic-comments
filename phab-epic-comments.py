#!/usr/bin/env python

import argparse
import os
import subprocess
import sys
import time
import tempfile
from datetime import datetime, timedelta
from phabricator import Phabricator


def extract_comments(transactions, since_epoch, until_epoch):
    comments = []
    for t in transactions:
        epoch = int(t['dateCreated'])
        if t['comments'] and epoch >= since_epoch and epoch <= until_epoch:
            comments.insert(0, t['comments'])
    return comments


def pprint_comments(task, comments, print_empty, f):
    if not print_empty and not comments:
        return
    f.write("\n# " + epics[task] + "\n")
    for c in comments:
        f.write("\n" + c + "\n")
    if not comments:
        f.write("No progress" + "\n")
    f.write("\n")
    f.write("https://phabricator.collabora.com/T" + str(task) + "\n")


if __name__ == "__main__":
  
  parser = argparse.ArgumentParser(description='Retrieve all coments in a given week from EPIC tasks')
  parser.add_argument('-p', '--project', type=str, required=True,
                      help='the project to retrieve the comments')
  parser.add_argument('-w', '--week', type=int, required=True,
                      help='the number of the week to retrieve the comments')
  parser.add_argument('-y', '--year', type=int, default=datetime.now().year,
                      help='the year to retrieve the comments')
  parser.add_argument('-a', '--all', default=False, action='store_const', const=True,
                      help="also list tasks that weren't updated")
  parser.add_argument('-o', '--output', type=str, default=None,
                      help='output to a file instead of stdout')
  args = parser.parse_args()
  phab = Phabricator()  # This will use your ~/.arcrc file
  phab.user.whoami()
  
  # year, first day of the week, and number of the week
  s_date = datetime.strptime("%s-1-%s" % (args.year, args.week), "%Y-%w-%W")
  s_epoch = int(s_date.strftime('%s'))
  e_date = datetime.strptime("%s-1-%s" % (args.year, args.week + 1), "%Y-%w-%W")
  e_date = e_date - timedelta(seconds=1)
  e_epoch = int(e_date.strftime('%s'))

  if args.output:
      fd, tmp_file_name = tempfile.mkstemp(suffix=".md")
      f = open(tmp_file_name, "w")
  else:
      f = sys.stdout
  
  f.write("Report: week %s / from %s to %s\n" % (args.week, str(s_date), str(e_date)))
  f.write("-----------------------------------------------------------------\n")

  query = {
    "constraints": {
      "name": args.project
    }
  }

  project_phid = phab.project.search(**query).response["data"][0]["phid"]
  
  query = {
    "queryKey": "open",
    "constraints": {
      "projects": [project_phid]
    }
  }
  tasks = phab.maniphest.search(**query).data
  epics = {}
  
  for task in tasks:
      if "EPIC" in task["fields"]["name"]:
          epics[task["id"]] = task["fields"]["name"]
  
  d_transactions = phab.maniphest.gettasktransactions(ids=epics.keys()).response
  
  for task in d_transactions:
      comments = extract_comments(d_transactions[task], s_epoch, e_epoch)
      pprint_comments(int(task), comments, args.all, f)
  if f is not sys.stdout:
      f.close()
      os.close(fd)

  if args.output:
    try:
        subprocess.check_call(["pandoc", tmp_file_name, "-o", args.output])
        os.remove(tmp_file_name)
    except OSError:
        sys.stderr.write("WARNING: pandoc is not installed, could not convert the file format.")
        os.rename(tmp_file_name, args.output)
